using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using dotnet_core_test.Models;

namespace dotnet_core_test.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase {

        private static string[] _usernames = new string[] {
            "James",
            "Joe",
            "Bryan",
            "Dexter",
            "Sal"
        };

        public UserController () { }

        // GET api/user
        [HttpGet ("")]
        public ActionResult<IEnumerable<string>> Getstrings () {
            return _usernames;
        }

        // GET api/user/5
        [HttpGet ("{id}")]
        public ActionResult<string> GetstringById (int id) {
            return null;
        }

        // POST api/user
        [HttpPost ("")]
        public void Poststring (string value) { }

        // PUT api/user/5
        [HttpPut ("{id}")]
        public void Putstring (int id, string value) { }

        // DELETE api/user/5
        [HttpDelete ("{id}")]
        public void DeletestringById (int id) { }
    }
}